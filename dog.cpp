///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file dog.cpp
/// @version 1.0
///
/// Exports data about all dogs
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo 15_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "dog.hpp"

using namespace std;

namespace animalfarm {
	
Dog::Dog( string newName, enum Color newColor, enum Gender newGender ) {
	gender = newGender;         /// Get from the constructor... not all dogs are the same gender (this is a has-a relationship)
	species = "Canis lupus";    /// Hardcode this... all dogs are the same species (this is a is-a relationship)
	hairColor = newColor;       /// A has-a relationship, so it comes through the constructor
	gestationPeriod = 64;       /// An is-a relationship, so it's safe to hardcode.  All dogs have the same gestation period.
	name = newName;             /// A has-a relationship.  Every dog has its own name.
}


const string Dog::speak() {
	return string( "Woof" );
}


/// Print our Dog and name first... then print whatever information Mammal holds.
void Dog::printInfo() {
	cout << "Dog Name = [" << name << "]" << endl;
	Mammal::printInfo();
}

} // namespace animalfarm

