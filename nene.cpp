///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @02_27_2021
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {
	
Nene::Nene(string newTagID, enum Color newColor, enum Gender newGender ) {
	gender = newGender;         /// Get from the constructor... not all nunu are the same gender (this is a has-a relationship)
	species = "Branta sandvicensis";    /// Hardcode this... all nunu are the same species (this is a is-a relationship)
	featherColor = newColor;       /// A has-a relationship, so it comes through the constructor
	tagID = newTagID; 

   migrat = string("true");
	//name = newName;             /// A has-a relationship.  A nunu can be native or nonnative.
}

const string Nene::speak(){
   return string("Nay, nay");
}
/// Print our Nunu and nativety first... then print whatever information Fish holds.
void Nene::printInfo() {
	cout << "Nene" << endl;
   cout << "Tag ID = ["<< tagID<<"]" << endl;
	Bird::printInfo();
}

} // namespace animalfarm


