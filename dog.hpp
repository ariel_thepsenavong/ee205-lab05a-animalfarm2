///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file dog.hpp
/// @version 1.0
///
/// Exports data about all dogs
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @02_15_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include <string>

#include "mammal.hpp"

using namespace std;


namespace animalfarm {

class Dog : public Mammal {
public:
	string name;
	
	Dog( string newName, enum Color newColor, enum Gender newGender );
	
	virtual const string speak();

	void printInfo();
};

} // namespace animalfarm

