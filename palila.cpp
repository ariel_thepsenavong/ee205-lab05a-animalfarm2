///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   02_26_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {
	
Palila::Palila(string whereFound, enum Color newColor, enum Gender newGender ) {
	gender = newGender;         /// Get from the constructor... not all nunu are the same gender (this is a has-a relationship)
	species = "Loxioides bailleui";    /// Hardcode this... all nunu are the same species (this is a is-a relationship)
	featherColor = newColor;       /// A has-a relationship, so it comes through the constructor
	migrat = string("false"); 

   isFound = whereFound;
	//name = newName;             /// A has-a relationship.  A nunu can be native or nonnative.
}


/// Print our Nunu and nativety first... then print whatever information Fish holds.
void Palila::printInfo() {
	cout << "Palila" << endl;
   cout << "Where Found = ["<<isFound<<"]" << endl;
	Bird::printInfo();
}

} // namespace animalfarm


