///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author @Ariel Thepsenavong <arielat@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   02_21_2021
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "aku.hpp"

using namespace std;

namespace animalfarm {
	
Aku::Aku(float weight, enum Color newColor, enum Gender newGender ) {
	gender = newGender;         /// Get from the constructor... not all nunu are the same gender (this is a has-a relationship)
	species = "Katsuwonus pelamis";    /// Hardcode this... all nunu are the same species (this is a is-a relationship)
	scaleColor = newColor;       /// A has-a relationship, so it comes through the constructor
	favoriteTemp = 75;       /// An is-a relationship, so it's safe to hardcode.  All cats have the same gestation period.
	//name = newName;             /// A has-a relationship.  A nunu can be native or nonnative.

   newWeight = weight;
}


/// Print our Nunu and nativety first... then print whatever information Fish holds.
void Aku::printInfo() {
	cout << "Aku" << endl;
   cout << "Weight = ["<< newWeight <<"]" << endl;
	Fish::printInfo();
}

} // namespace animalfarm

